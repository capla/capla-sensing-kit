Acknowledgements
================

This project is a continuation of the `Rural Hack Kit`_.

The ``CaplaSensingKit`` Arduino library bundles the following dependencies:

* `ModbusMaster`_
* `SimpleDHT`_

.. _Rural Hack Kit: https://github.com/OfficineArduinoTorino/RuralHack/
.. _ModbusMaster: https://github.com/4-20ma/ModbusMaster
.. _SimpleDHT: https://github.com/winlinvip/SimpleDHT