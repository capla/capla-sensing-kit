CAPLA Sensing Kit
=================

Thank you for visiting the documentation for the University of Arizona's
College of Architecture, Planning and Landscape Architecture's Arduino
sensing kit. 

.. toctree::
   :maxdepth: 2
   
   getting-started
   tutorial
   acknowledgements