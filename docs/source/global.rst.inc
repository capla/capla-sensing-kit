.. _Adafruit log-scale light sensor: https://www.adafruit.com/product/1384
.. _Getting started with the Arduino MKR1000: https://www.arduino.cc/en/Guide/MKR1000
.. _DHT22: https://www.adafruit.com/product/385
.. _Arduino Create: https://create.arduino.cc/editor