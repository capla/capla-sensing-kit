.. _tutorial:

Tutorial
========

.. toctree::
   :maxdepth: 1
   
   tutorial/light-sensor-diy
   tutorial/light-sensor-library
   tutorial/temp-and-humidity-sensor
   tutorial/soil-moisture-sensor
   tutorial/putting-it-all-together
   tutorial/recording-data
   tutorial/adding-other-sensors
   