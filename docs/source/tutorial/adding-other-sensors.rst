Adding Other Sensors
====================

If you'd like to add other sensors, you are free to use them in conjunction
with the CAPLA sensing kit library. For example, this sketch uses the library
to control the soil moisture and temperature/humidity sensor, but directly
controls the analog light sensor; you can use it as a template for adding
other sensors.

.. literalinclude:: ../../../examples/AddingOtherSensors/AddingOtherSensors.pde
   :language: cpp