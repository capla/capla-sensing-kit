Light Sensor Part 1: DIY
========================

You'll first need to solder a set of headers onto the Adafruit log-scale
light sensor. If you don't have experience soldering, don't panic. It's
actually quite simple and I'd recommend you read through this guide: `How to
Solder`_.

The sensor should have come with a breakaway header with six pins on it.
Break it in half so that you have three pins and then solder the headers onto
the three (VCC, OUT, GND) terminals of the sensor. Now you can put the sensor
into your breadboard and wire it up according to this diagram (it may help to
right click and and select "View Image", or to download the image so you can
see the detail more closely):

.. image:: ../_static/img/log_scale_light_sensor.png

This analog sensor outputs integer values that are not actually meaningful.
In order to use the readings, you need to convert the raw output of the sensor
to lux (the SI unit of illuminance). The function ``rawToLux`` handles this
conversion.

.. code:: cpp
   
   /*
    Example sketch using the Adafruit log-scale light sensor.
   */
   
   const int SENSOR_PIN = A0;
   const float SENSOR_READ_FREQUENCY = 2000;
   const float RAW_RANGE = 1024.0;
   const float LOG_RANGE = 5.0;
   
   void setup() {
       Serial.begin(9600);
   }
   
   void loop() {
       // Read the value.
       int rawValue = analogRead(SENSOR_PIN);
       float lux = rawToLux(rawValue);
   
       // Print the value.
       Serial.print("Light: ");
       Serial.print(rawToLux(rawValue));
       Serial.println(" lux");
   
       // Wait SENSOR_READ_FREQUENCY milliseconds before
       // reading again.
       delay(SENSOR_READ_FREQUENCY);
   }
   
   float rawToLux(int rawValue) {
       return pow(10, (rawValue * LOG_RANGE) / RAW_RANGE);
   }

.. _How to Solder: https://www.makerspaces.com/how-to-solder/
