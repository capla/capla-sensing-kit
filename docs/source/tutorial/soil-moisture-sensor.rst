Soil Sensor
===========

Lot of wires involved with this one. You'll also need to get your TTL UART to
RS485 converter.

Preparing the Sensor
--------------------

This sensor comes in "non-ruggedized" and "ruggedized" verisons. The
ruggedized one has wires pre-soldered to it, heat-shrink tubing covering the
upper third of the sensor, and is dipped in epoxy for weather-proofing. The
non-ruggedized one is just a flat, black stick. If it isn't obvious that you
have the ruggedized version (the biggest giveaway is the long white wire
coming off one side) you have the non-ruggedized. Follow the instructions in the
appropriate section, below.

Ruggedized Sensor
~~~~~~~~~~~~~~~~~

1. Strip about two inches of the white housing off of the end of the cable coming
   out of the Chirp! soil sensor.
2. You should have exposed four wires: a red, a black, a yellow and a green. If
   you see less than four wires, you probably cut one when you were stripping the
   outer housing. That's okay--cut all of them flush with the white housing and
   return to (1). If you've got all four, strip about a centimeter of housing off
   the end of each one.
3. There is a green, plastic block on the RS485 converter with two screw terminals.
   These are the A and B terminals for an RS485 connection. The yellow wire of the
   soil sensor goes into the A terminal (loosen the screw, slide the wire in, and
   tighten the screw), and the green wire goes into the B terminal (see the diagram
   below).

Non-ruggedized Sensor
~~~~~~~~~~~~~~~~~~~~~

1. Solder wires onto four terminals on the sensor (recommended wire colors
   are in parenthesis, you can use others but the diagram later on assumes
   that you've used these).

   a. VCC (Red)
   b. GND (Black)
   c. B (Green)
   d. A (Yellow)

Wiring the Sensor
-----------------

The circuit should be wired according to this diagram:

.. note::
   You may face (at least!) two additional difficulties: 1) It's difficult to
   get the stranded wire from the sensor to fit into the breadboard--if you
   apply a bit of solder to the wire it will stiffen it and make it easier;
   2) Both ends of the RS485 converter won't fit into a single breadboard but
   you'll need to connect the VCC and GND terminals to power and ground,
   respectively. You could use a male-to-female jumper cable or a second
   breadboard; there's likely many more, creative solutions!

.. note::
   Unlike the previous sensors we've worked with, the soil sensor connects to the
   MKR1000's 5V power line (not its VCC one).

.. image:: ../_static/img/soil_sensor.png

The following script should get the sensor running. The moisture reading that
the sensor reports is a number--mine gets something around 217 in the air and
590 submerged in water. These numbers will vary widely depending on the capacitance
of the particular soil you're measuring so you'll need to collect reference measurements
using soil samples before you can draw any useful conclusion.

.. literalinclude:: ../../../examples/ChirpSoilSensorRS485/ChirpSoilSensorRS485.pde
   :language: cpp