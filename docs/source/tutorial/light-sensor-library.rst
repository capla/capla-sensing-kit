Light Sensor Part 2: Library
============================

The light sensor is simple to use but other devices, like the soil sensor
you'll use later, are more complex--both in the way that they need to be
connected to the Arduino and also in the software you'll need to write to use
them.

From this point foward you'll use the ``CaplaSensingKit`` library to
interface with your sensors. `Download the library here`_ and install it in
your IDE (for the web IDE see: `Importing a Custom Library`_ and for the
desktop IDE see: `Importing a .zip Library`_).

.. note::
   There are example sketches included in the library for each sensor that you
   can load if you're having trouble.

The light sensor should still be wired up like this (no change from before!):

.. image:: ../_static/img/log_scale_light_sensor.png

And if the library was installed properly you should be able to upload this sketch
and get the same results as before.

.. literalinclude:: ../../../examples/AdafruitLogScaleLightSensor/AdafruitLogScaleLightSensor.pde
   :language: cpp

``CaplaSensingKit`` Library Patterns
------------------------------------

Use of sensors from the ``CaplaSensingKit`` library will always follow this
pattern:

First, the sensor interface is initialized. This happens outside of ``void
setup()`` and ``void loop()``, and should usually happen somewhere near the
top of your sketch. We pass the pin(s) that the sensor uses as arguments to
the constructor:

.. literalinclude:: ../../../examples/AdafruitLogScaleLightSensor/AdafruitLogScaleLightSensor.pde
   :language: cpp
   :lines: 11
   :emphasize-lines: 1

Next, in ``void setup()``, we call the ``setup`` method on the sensor interface.

.. literalinclude:: ../../../examples/AdafruitLogScaleLightSensor/AdafruitLogScaleLightSensor.pde
   :language: cpp
   :lines: 13-16
   :emphasize-lines: 3

Finally, in ``void loop()``, we read from the sensor. In the first
highlighted line we "declare" the variable ``float lux;`` In the second
highlighted line we pass a pointer to this variable to the ``read()`` method
(``&lux`` is a pointer to ``lux``). If you're curious to learn more about
pointers and pass-by-pointer functions, this `IBM Knowledge Center article`_
may get you started in the right direction. If not, don't worry--you're in
good company.

.. literalinclude:: ../../../examples/AdafruitLogScaleLightSensor/AdafruitLogScaleLightSensor.pde
   :language: cpp
   :lines: 18-30
   :emphasize-lines: 3,4

.. _Download the library here: https://gitlab.com/capla/capla-sensing-kit/-/archive/master/capla-sensing-kit-master.zip
.. _Importing a Custom Library: https://create.arduino.cc/projecthub/Arduino_Genuino/import-your-sketchbook-and-libraries-to-the-web-editor-296bb3#toc-importing-a-custom-library-4
.. _Importing a .zip Library: https://www.arduino.cc/en/guide/libraries#toc4
.. _IBM Knowledge Center article: https://www.ibm.com/support/knowledgecenter/SSLTBW_2.1.0/com.ibm.zos.v2r1.cbclx01/pass_by_pointer.htm
