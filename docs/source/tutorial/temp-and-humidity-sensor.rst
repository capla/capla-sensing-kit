Temperature and Humidity Sensor
===============================

Setting up the DHT22 temperature and humidity sensor is straightforward:

.. image:: ../_static/img/dht22.png

And the code to use it:

.. literalinclude:: ../../../examples/DHT22/DHT22.pde
   :language: cpp