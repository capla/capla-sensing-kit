Putting it all Together: Multiple Sensors
=========================================

Now you should try reading from all three sensors at once.

.. image:: ../_static/img/all_sensors.png

Once you've done that, upload this script:

.. note::
   You can change the ``SENSOR_READ_FREQUENCY`` but you shouldn't go below
   2000 (milliseconds). The DHT22 has a sampling frequency of 0.5Hz which
   means it shouldn't be polled (measured) more than once every two seconds.

.. literalinclude:: ../../../examples/MultipleSensors/MultipleSensors.pde
   :language: cpp