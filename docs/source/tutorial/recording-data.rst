Recording Data
==============

Finally, we will use the Arduino IoT cloud to log and record data from your assembled
sensor package.

.. warning::
   The Arduino IoT Cloud is in beta and may change unexpectedly. If the
   instructions on this page seem wrong in some way, it's likely that the IoT
   platform was updated. If that's the case, reference the `Arduino IoT Cloud
   Tutorial`_--hosted by the Arduino organization, it should remain up to
   date.

Registering your Arduino MKR1000
--------------------------------

First, go to the `Arduino IoT Cloud`_ website. You'll need to create an
account if you don't already have one. Once you've signed in:

1. Click the "Add New Thing" button;

2. Select "Set up a MKR1000";

3. If necessary, download and install the "Arduino Create Plugin" that the
   website prompts you to install;

4. Plug your Arduino MKR1000 into the USB port of your computer;

5. Follow the rest of the instructions in the wizard--if the "Crypto Chip"
   configuration fails, you may try pressing the RST button on the MKR1000 twice
   and then attempting the configuration again (this was necessary for me);

6. After you've successfully registered your Arduino, you can click the "Back
   to Cloud" button and continue to the next section.

Creating a New "Thing"
----------------------

After completing the last section, you should be returned to the "Create New
Thing" wizard. If not, go to the `Arduino IoT Cloud`_ and click "Add New
Thing". Then:

1. Enter a name for your thing and click "Create"

2. Click the "Add Property" button and set up the ``temperature`` property
   like so:

   .. image:: ../_static/img/property_configuration.png

3. You'll also need to add properties for ``soilMoisture``, ``humidity`` and
   ``lux``. Ensure that the variable name appears exactly as they are
   spelled here (you can name them whatever you'd like). Choose sensible
   settings for each property's type and min/max values.

Uploading Data
--------------

After all four properties have been added. Click the "Edit Sketch" button to
go to the Arduino Web Editor.

1. If you haven't already installed the CAPLA sensing kit library to the
   Arduino Web IDE, `download the library here`_ and then follow the `Importing
   a Custom Library`_ guide.

2. Go to the "Secret" tab in the IDE. Enter the name (``SECRET_SSID``) and
   password (``SECRET_PASS``) of your wireless network.

3. Go back to the main sketch tab and paste this script into the editor:

   .. literalinclude:: ../_static/scripts/ArduinoCloud.pde
      :language: cpp

4. Finally, click the "Upload and Save" button (arrow icon) to upload the
   script to your Arduino. After the script has successfully uploaded, click
   the "Go to IoT Cloud" button.

Viewing Data
------------

With your "Thing" selected in the IoT Cloud, navigate to the "Dashboard" tab.
You should see the current reading of each sensor and, if you click the lil'
graph icon for each one, you can view the historic values that sensor recorded.

.. image:: ../_static/img/iot_dashboard.png

.. note::
   At this point you can use the Arduino without a computer! You can use a USB
   power supply or a battery to power it.

.. _Arduino IoT Cloud: https://create.arduino.cc/iot/
.. _Arduino IoT Cloud Tutorial: https://create.arduino.cc/projecthub/133030/iot-cloud-getting-started-c93255
.. _download the library here: https://gitlab.com/capla/capla-sensing-kit/-/archive/master/capla-sensing-kit-master.zip
.. _Importing a Custom Library: https://create.arduino.cc/projecthub/Arduino_Genuino/import-your-sketchbook-and-libraries-to-the-web-editor-296bb3#toc-importing-a-custom-library-4
