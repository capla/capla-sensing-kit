.. include:: global.rst.inc

.. _getting_started:

Getting Started
===============

Prior Knowledge
---------------

The CAPLA sensing kit is built around the Arduino MKR1000. If you're using the online
Arduino IDE, `Arduino Create`_, you don't need to install anything in order to upload
software to the MKR1000. If you're using the desktop Arduino IDE you'll need to follow
the `Getting started with the Arduino MKR1000`_ guide on the Arduino website. 

The :ref:`tutorial` in this guide will walk you through assembling a sensing package
that is capable of recording ambient light levels, temperature, humidity and soil
moisture. It assumes that you already have some experience writing sketches in the
Arduino programming language and some experience wiring circuits on a breadboard.

If you don't, I recommend that you read the following guides:

* `What is a Sketch`_
* `Introduction to Variables`_
* `How to use a breadboard`_

Supplies List
-------------

You'll need the following supplies to complete this project:

* Arduino MKR1000
* `Adafruit log-scale light sensor`_
* `DHT22`_ temperature and humidity sensor
* Chirp! RS485 soil sensor
   - There is a sensor based around the I2C protocol that looks very similar
     but will not work with the code in this guide. Check that your sensor has
     the URL https://github.com/Miceuz/rs485-moist-sensor printed on it.
* `MAX485 TTL UART to RS485 Converter`_
* Breadboard
* Soldering iron
* Miscellaneous jumper wires

.. _How to use a breadboard: https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard/all
.. _What is a Sketch: https://www.arduino.cc/en/Tutorial/Sketch
.. _Introduction to Variables: https://www.arduino.cc/en/Tutorial/Variables

Getting Help or Offering Feedback
---------------------------------

If something is confusing or if you get stuck for a particularly long time,
please `write to me`_ (nicwolf1@gmail.com). I've tried to structure things
so that they are challenging but not frustrating--I'm sure that somewhere
I've missed the mark or made some other, egregious mistake.

.. _write to me: nicwolf1@gmail.com
.. _MAX485 TTL UART to RS485 Converter: https://core-electronics.com.au/ttl-uart-to-rs485-converter-module.html