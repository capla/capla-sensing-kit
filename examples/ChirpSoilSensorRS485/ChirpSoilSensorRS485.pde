/*
 Using the CAPLA Sensing library to control the Chirp! RS485
 soil moisture sensor.
*/

#include <CaplaSensingKit.h>

const int SOIL_SENSOR_PIN_DE = 3;
const int SOIL_SENSOR_PIN_RE = 4;
const float SENSOR_READ_FREQUENCY = 2000;

ChirpSoilSensorRS485 soilSensor(
    SOIL_SENSOR_PIN_DE,
    SOIL_SENSOR_PIN_RE
);

void setup() {
    Serial.begin(9600);
    soilSensor.setup();
}

void loop() {
    // Read from the sensor.
    float soilMoisture;
    soilSensor.read(&soilMoisture);

    // Print the soil moisture.
    Serial.print("Soil Moisture: ");
    Serial.println(soilMoisture);
    
    // Wait SENSOR_READ_FREQUENCY milliseconds before reading again.
    delay(SENSOR_READ_FREQUENCY);
}