
/*
 Streaming data to Microsoft Excel from the Chirp! RS485 soil
 sensor, Adafruit log-scale light sensor, and the DHT22 temperature
 and humidity sensor.
*/

#include <CaplaSensingKit.h>

// Create an interface for the DHT22
const int DHT22_PIN = 7;
DHT22 dht22(DHT22_PIN);

// Create an interface for the Adafruit log-scale light sensor
const int LIGHT_SENSOR_PIN = A0;
AdafruitLogScaleLightSensor lightSensor(LIGHT_SENSOR_PIN);

// Create an interface for Chirp! soil sensor
const int SOIL_SENSOR_PIN_DE = 3;
const int SOIL_SENSOR_PIN_RE = 4;
ChirpSoilSensorRS485 soilSensor(
    SOIL_SENSOR_PIN_DE,
    SOIL_SENSOR_PIN_RE
);

const float SENSOR_READ_FREQUENCY = 2000;

void setup() {
    Serial.begin(9600);
    dht22.setup();
    lightSensor.setup();
    soilSensor.setup();
}

void loop() {
    // Read from the sensors.
    float humidity;
    float temperature;
    dht22.read(&temperature, &humidity);

    float lux;
    lightSensor.read(&lux);

    float soilMoisture;
    soilSensor.read(&soilMoisture);

    // Print the readings.
    Serial.print(temperature);
    Serial.print(",");
    Serial.print(humidity);
    Serial.print(",");
    Serial.print(lux);
    Serial.print(",");
    Serial.print(soilMoisture);
    Serial.println(); // Add a blank line after this printout
    
    // Wait SENSOR_READ_FREQUENCY milliseconds before reading again.
    delay(SENSOR_READ_FREQUENCY);
}
