
/*
 Using the CAPLA Sensing library to control the Chirp! RS485 soil sensor, and
 the DHT22 temperature and humidity sensor. Manually control the analog light
 sensor.
*/

#include <CaplaSensingKit.h>

// Set up the light sensor without using the CAPLA sensing library.
const int LIGHT_SENSOR_PIN = A0;
const float LIGHT_SENSOR_RAW_RANGE = 1024.0;
const float LIGHT_SENSOR_LOG_RANGE = 5.0;


// Create an interface for the DHT22 using the library
const int DHT22_PIN = 7;
DHT22 dht22(DHT22_PIN);

// Create an interface for Chirp! soil sensor using the library
const int SOIL_SENSOR_PIN_DE = 3;
const int SOIL_SENSOR_PIN_RE = 4;
ChirpSoilSensorRS485 soilSensor(
    SOIL_SENSOR_PIN_DE,
    SOIL_SENSOR_PIN_RE
);

const float SENSOR_READ_FREQUENCY = 2000;

void setup() {
    Serial.begin(9600);
    dht22.setup();
    soilSensor.setup();
    // No setup necessary for the analog light sensor
}

void loop() {
    // Read from the sensors.
    float humidity;
    float temperature;
    dht22.read(&temperature, &humidity);

    // Read from the light sensor manually
    int rawLightSensorValue = analogRead(LIGHT_SENSOR_PIN);
    float lux = lightSensorRawToLux(rawLightSensorValue);

    float soilMoisture;
    soilSensor.read(&soilMoisture);

    // Print the readings.
    Serial.println("Reading");
    Serial.println("==============================");

    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.println(" *C");

    Serial.print("Humidity: ");
    Serial.print(humidity);
    Serial.println(" %RH");

    Serial.print("Light: ");
    Serial.print(lux);
    Serial.println(" lux");

    Serial.print("Soil Moisture: ");
    Serial.println(soilMoisture);

    Serial.println(); // Add a blank line after this printout
    
    // Wait SENSOR_READ_FREQUENCY milliseconds before reading again.
    delay(SENSOR_READ_FREQUENCY);
}

float lightSensorRawToLux(int rawValue) {
    return pow(10, (rawValue * LIGHT_SENSOR_LOG_RANGE) / LIGHT_SENSOR_RAW_RANGE);
}