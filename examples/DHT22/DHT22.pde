/*
 Using the CAPLA Sensing library to control the DHT22 temperature
 and humidity sensor.
*/

#include <CaplaSensingKit.h>

const int DHT22_PIN = 7;
const float SENSOR_READ_FREQUENCY = 2000;

DHT22 dht22(DHT22_PIN);

void setup() {
    Serial.begin(9600);
    dht22.setup();
}

void loop() {
    // Read from the sensor.
    float humidity;
    float temperature;
    dht22.read(&temperature, &humidity);
    
    // Print the temperature...
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.println(" *C");
    // and the humidity.
    Serial.print("Humidity: ");
    Serial.print(humidity);
    Serial.println(" %RH");
    
    // Wait SENSOR_READ_FREQUENCY milliseconds before reading again.
    delay(SENSOR_READ_FREQUENCY);
}