/*
 Using the CAPLA Sensing library to control the Adafruit
 log-scale light sensor.
*/

#include <CaplaSensingKit.h>

const int LIGHT_SENSOR_PIN = A0;
const float SENSOR_READ_FREQUENCY = 2000;

AdafruitLogScaleLightSensor lightSensor(LIGHT_SENSOR_PIN);

void setup() {
    Serial.begin(9600);
    lightSensor.setup();
}

void loop() {
    // Read from the sensor.
    float lux;
    lightSensor.read(&lux);

    // Print the lux reading.
    Serial.print("Light: ");
    Serial.print(lux);
    Serial.println(" lux");
    
    // Wait SENSOR_READ_FREQUENCY milliseconds before reading again.
    delay(SENSOR_READ_FREQUENCY);
}