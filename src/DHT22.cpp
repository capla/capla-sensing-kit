#include "DHT22.h"

DHT22::DHT22(int pin) : pin { pin } {} 

void DHT22::setup() { }

void DHT22::read(float *temperature, float *humidity)
{
    dht22.read2(pin, temperature, humidity, NULL);
}