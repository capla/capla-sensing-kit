#ifndef AdafruitLogScaleLightSensor_h
#define AdafruitLogScaleLightSensor_h

#include <Arduino.h>

class AdafruitLogScaleLightSensor {
private:
    int pin;
    float rawReadingToLux(int rawReading);
public:
    AdafruitLogScaleLightSensor(int pin);
    void setup();
    void read(float *lux);
};

#endif