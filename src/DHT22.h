#ifndef DHT22_h
#define DHT22_h

#include "dependencies/SimpleDHT/SimpleDHT.h"

class DHT22 {
private:
    int pin;
    SimpleDHT22 dht22;
public:
    DHT22(int pin);
    void setup();
    void setPin(int pin) { this->pin = pin; };
    void read(float *temperature, float *humidity);
};

#endif