#ifndef SoilMoisture_h
#define SoilMoisture_h

#include "dependencies/ModbusMaster/ModbusMaster.h"
#include <float.h>

class ChirpSoilSensorRS485 {
private:
    int pinRe;
    int pinDe;
    int baudrate;
    int parity;
    ModbusMaster node;
public:
    ChirpSoilSensorRS485(int pinDe, int pinRe);
    int setup();
    void read(float *moisture);
};

#endif