#include "AdafruitLogScaleLightSensor.h"

#define LOG_RANGE 5.0
#define RAW_RANGE 1024.0

AdafruitLogScaleLightSensor::AdafruitLogScaleLightSensor(int pin)
    : pin { pin }
{
}

void AdafruitLogScaleLightSensor::setup()
{
}

void AdafruitLogScaleLightSensor::read(float *lux)
{
    int rawReading = analogRead(pin);
    *lux = rawReadingToLux(rawReading);
}

float AdafruitLogScaleLightSensor::rawReadingToLux(int rawReading)
{
    return pow(10, (rawReading * LOG_RANGE) / RAW_RANGE);
}